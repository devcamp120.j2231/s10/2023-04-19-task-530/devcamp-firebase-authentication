import logo from './logo.svg';
import './App.css';
import auth from './firebase';
import { GoogleAuthProvider, onAuthStateChanged, signInWithPopup, signOut } from 'firebase/auth';
import { useEffect, useState } from 'react';

function App() {
  const [user, setUser] = useState(null);

  const provider = new GoogleAuthProvider();

  const signInWithGoogle = () => {
    signInWithPopup(auth, provider)
    .then((result) => {
      console.log(result);
      setUser(result.user);
    })
    .catch((error) => {
      console.log(error);
      setUser(null);
    })
  }

  const signOutGoogle = () => {
    signOut(auth)
    .then(() => {
      console.log("sign out successfully!");
      setUser(null);
    })
    .catch((error) => {
      console.log(error);
      setUser(null);
    })
  }

  useEffect(() => {
    onAuthStateChanged(auth, (loggedUser) => {
      if (loggedUser) {
        console.log("already logged in");
      } else {
        console.log("have not logged in");
      }
      setUser(loggedUser);      
    });          
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {
          user ?
          <>
            <img src={user.photoURL} style={{width:"100px", borderRadius:"50%", marginTop:"10px"}}/>
            <p>Welcome, {user.displayName}</p>
            <button onClick={signOutGoogle}>Sign out</button>          
          </>
          :
          <>
            <p>Please sign in!</p>
            <button onClick={signInWithGoogle}>Sign in with google</button>          
          </>
        }
      </header>
    </div>
  );
}

export default App;
