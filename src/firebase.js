// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDw6IqTdygp69gePb9JdtEjV4U7H1TIRjU",
  authDomain: "r2231-67c18.firebaseapp.com",
  projectId: "r2231-67c18",
  storageBucket: "r2231-67c18.appspot.com",
  messagingSenderId: "797945578823",
  appId: "1:797945578823:web:4d4a42443e01c52570f862"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth();

export default auth;
